import React from "react";
import './Mainthree.css';
import Headert from "./Headert";
import img20 from '../Images/circle.png';
import img21 from '../Images/batch.png';
import img22 from '../Images/phone.png';
import img23 from '../Images/mail.png';
import img24 from '../Images/down.png';
import img25 from '../Images/lock.png';
import img26 from '../Images/pen.png';
import img27 from '../Images/dustbin.png';

// import Maintwo from './Components/Maintwo';



function Mainthree() {
    return (
        <>


            <Headert />


            <div className="bigout">

                {/* section one */}

                <div className="Sec1img">
                    <img src={img20} alt="" className="img20" />
                    <img src={img21} alt="" className="img21" />
                </div>

                <div className="sec1">
                    <h1 className="clgname">Malaysia College Of Engineering</h1>
                    <h3>No.19 1A Jin Pandan 3/9 Taman Pandan Jaya</h3>
                    <h3>Malaysia</h3>
                </div>

                <div className="ul"></div>

                <p className="para0">Transforming Dreams into reality with malaysia</p>


                <div className="sect1">
                    <img src={img22} alt="" className="img22" />
                    <h1 className="phno"> 672-434-4222 </h1>
                </div>

                <div className="secti1">
                    <img src={img23} alt="" className="img23" />
                    <h1 className="mail"> malaysiauniversity@ac.in</h1>
                </div>

                {/* section two */}

                <div className="sec2">
                    <div>
                        <h1 className="ov">Overview</h1>
                    </div>

                    <div>
                        <img src={img24} alt="" className="img24" />
                        <h1 className="om">Only me</h1>
                        <img src={img25} alt="" className="img25" />
                    </div>

                    <div className="ul1"></div>

                    <div>
                        <p className="lorem">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut <br></br>labore et dolore magna aliqua.
                            Ut enim ad minim veniastrud exercitation ullamco laboris
                            nisi ut <br></br>aliquip ex ea commodo consequat. Duis autere dolor in reprehenderit in voluptate
                            velit esse<br></br> cillum dolore eu fugiat nulla pariatur.</p>
                    </div>

                    <div className="ul2"></div>

                    <div>
                        <h1 className="web">Website</h1>
                        <a href="https://www.mu.club" className="weblink">https://www.mu.club</a>

                        <h1 className="indus">Industry</h1>
                        <p className="em">Education Management</p>

                        <h1 className="hq">Headquaters</h1>
                        <p className="jm">Jaya, Malaysia</p>

                        <p className="cng">Change</p>

                        <h1 className="cs">Company Size</h1>
                        <p className="employ">201-500 employees</p>

                        <h1 className="found">Founded</h1>
                        <p className="year">2017</p>
                    </div>
                </div>

                {/* section three */}

                <div className="sec3">

                    <div>
                        <h1 className="contact2">Contact Person</h1>
                    </div>

                    <div className="ul3"></div>

                    <div>
                        <p className="edit">Edit or add contact person for your organization changes will be sent to talent corp for approval.</p>
                        <p className="cls">Close</p>
                    </div>

                    <div className="table">
                        <table style={{ width: "87%" }}>

                            <tr style={{ height: "70px" }}>
                                <th>Contact Person</th>
                                <th>Job Title</th>
                                <th>Date Joined</th>
                                <th></th>
                            </tr>

                            <tr style={{ height: "75px" }}>
                                <td>Alia Zulaikha
                                    <br></br><p className="alia">alaizulaikha@qlco.com</p></td>
                                <td>HR Manager</td>
                                <td>2 Months</td>
                                <td>
                                    <img src={img26} alt="" className="img26" />
                                    <img src={img27} alt="" className="img27" />
                                </td>
                            </tr>

                            <tr style={{ height: "75px" }}>
                                <td>Rahmat Zain
                                    <br></br><p className="rahmat">rahmatzain@qlco.com</p></td>
                                <td>HR Manager</td>
                                <td>2 Months</td>
                                <td>
                                    <img src={img26} alt="" className="img26" />
                                    <img src={img27} alt="" className="img27" />
                                </td>
                            </tr>

                        </table>
                    </div>

                    <div className="">
                        <p className="addcnt">+ Add another contact person</p>
                    </div>

                </div>

                {/* section four */}

                <div className="sec4">

                    <div>                                            {/*section 3 css applied here */}
                        <h1 className="ov">Location</h1>
                    </div>

                    <div>
                        <img src={img24} alt="" className="img24" />
                        <h1 className="om">Only me</h1>
                        <img src={img25} alt="" className="img25" />
                    </div>

                    <div className="ul1"></div>

                    <div className="lasttab">

                        <table style={{ width: "87%" }} >
                            <tr style={{ height: "55px" }}>
                                <td className="primary">Primary
                                    <br></br><p className="address">No. 19 1A Jln Pandan 3/9 Taman Jaya <br></br>
                                        Malaysia 5600038, MA</p></td>

                                <td>
                                    <img src={img26} alt="" className="img26" />
                                    <img src={img27} alt="" className="img27" />
                                </td>
                            </tr>

                        </table>

                    </div>

                    <div className="">
                        <p className="addloc">+ Add secondary Location</p>
                    </div>

                </div>

            </div>



            {/* <Maintwo /> */}

            <div className="underlineoo"></div>

            <div className="locateoo">
                <h3>Locate Us</h3>
                <h3>6th floor, surian Tower, 1, jalan PJU7/3 Mutiara Damansara,</h3>
                <h3>47810 petaling jaya, selangor</h3>
            </div>

            <div className="contactoo">
                <h3>Contact Us</h3>
                <h3>+603 7839 7000</h3>
            </div>
        </>

    );
}


export default Mainthree;

