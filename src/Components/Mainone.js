import React from "react";
import './Mainone.css';
import img17 from '../Images/graph1.png';
import img18 from '../Images/boy.png';
import img19 from '../Images/dots1.png';
import img30 from '../Images/graph3.png';
import Header from "./Header";
import Maintwo from "./Maintwo";



function Mainone() {
    return (
        <>
          <Header/>
            <div className="border1">
                <p className="head1">Assessment result on average</p>
                <img src={img17} alt="" className="img17" />

                <ul className="list">
                    <li className="dot1">Ideas and oppurtunities</li>
                    <li className="dot2">Resource</li>
                    <li className="dot3">Into action</li>
                </ul>

                <div>
                    <img src={img18} alt="boy" className="img18" />
                </div>
            </div>

            <div className="border2">
                <div className="">
                    <h1 className="cb">Comparative Bar</h1>
                    <img src={img19} alt="" className="img19" />
                </div>



                <div>
                    <p className="numb1">43% EnglishProficiency 57%</p>
                    <label for="file"></label>
                    <progress id="file" value="43" max="100" className="bar1"></progress> 
                    <p className="planned">planned</p>
                    <p className="Notplanned">Not planned</p>
                </div>


                <div>
                    <p className="numb2">19% ITSkills 81%</p>
                    <label for="file"></label>
                    <progress id="file" value="19" max="100" className="bar2"></progress>
                    <p className="documented">Documented</p>
                    <p className="notdocumented">Not Documented</p>
                </div>

                
                <div>
                    <p className="numb3">68% employementFactor 32%</p>
                    <label for="file"></label>
                    <progress id="file" value="68" max="100" className="bar3"></progress>
                    <p className="Designed">Designed</p>
                    <p className="notDesigned">Not Designed</p>
                </div>

                <div>
                    <p className="numb4">94% otherFactor 6%</p>
                    <label for="file"></label>
                    <progress id="file" value="68" max="100" className="bar4"></progress>
                    <p className="coded">coded</p>
                    <p className="notcoded">Not coded</p>
                </div>



            </div>

            <div className="border3">
                <img src={img30} alt="" className="img30" />
            </div>
            <Maintwo/> 

        </>
    );
}


export default Mainone;