import React from "react";
import './Sidebar.css';
import img0 from '../Images/applogo.png';
import img1 from '../Images/db1.png';
import img2 from '../Images/db2.png';
import img3 from '../Images/user1.png';
import img4 from '../Images/mu1.png';
import img5 from '../Images/sett1.png';
import img6 from '../Images/sett2.png';
import img7 from '../Images/he1.png';
import img8 from '../Images/he2.png';
import img9 from '../Images/he3.png';

import { NavLink } from "react-router-dom";



function Sidebar() {

    const [click,setclick]= React.useState(true);
    const [secClick,setSecondClick]=React.useState(false);

    const handle = ()=>{
        setclick(!click);
        setSecondClick(!secClick);
    }
    return (
        <>


            <div className="outer" >
                <img src={img0} alt="" className="img0" />
                <p className="uni">University</p>
                    {/* <Link to="/" >      */}
                <div className="dashboard">
                <NavLink to="/" onClick={handle} >   
                    <img src={img1} alt="" className="img1" />
                    <img src={img2} alt="" className="img2" /> 
                    <h1 className={`db ${click && "active"}`}>Dashboard</h1> 
                    <div className="last"></div>
                    </NavLink>
                </div>
                {/* </Link> */}
              
                <div className="students" >
                    <img src={img3} alt="" className="img3" />
                    <h1 className="stu">Students</h1>
                </div>
               
                
                <div className="university">
                <NavLink  to="/two"  onClick={handle}>
                    <img src={img4} alt="" className="img4" />
                    <h1  className={`myuniv ${secClick && "secactive"}`} >My&nbsp;University</h1>
                    <div className="last2"></div>
                    </NavLink>
                </div>
               

                <div className="settings">
                    <img src={img5} alt="" className="img5" />
                    <img src={img6} alt="" className="img6" />
                    <h1 className="sett">Settings</h1> 
                </div>

                <div className="help">
                    <img src={img7} alt="" className="img7" />
                    <img src={img8} alt="" className="img8" />
                    <img src={img9} alt="" className="img9" />
                    <h1 className="hel">Help</h1> 
                </div>







            </div>

            <div className="bg"></div>






        </>
    );
}

export default Sidebar;
